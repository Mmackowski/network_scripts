
ip netns add N1
ip netns add N2
ip netns add N3
ip netns add N4

ovs-vsctl add-br br0

ip link add tap1 type veth peer name ovs-tap1
ip link add tap2 type veth peer name ovs-tap2
ip link add tap3 type veth peer name ovs-tap3
ip link add tap4 type veth peer name ovs-tap4

ovs-vsctl add-port br0 ovs-tap1 tag=100
ovs-vsctl add-port br0 ovs-tap2 tag=100
ovs-vsctl add-port br0 ovs-tap3 tag=200
ovs-vsctl add-port br0 ovs-tap4 tag=200


ip link set dev tap1 netns N1
ip link set dev tap2 netns N2
ip link set dev tap3 netns N3
ip link set dev tap4 netns N4

ip netns exec N1 ip link set dev tap1 up
ip netns exec N2 ip link set dev tap2 up
ip netns exec N3 ip link set dev tap3 up
ip netns exec N4 ip link set dev tap4 up

ip link set dev ovs-tap1 up
ip link set dev ovs-tap2 up
ip link set dev ovs-tap3 up
ip link set dev ovs-tap4 up

ip netns exec N1 ip addr add 10.0.0.1/24 dev tap1
ip netns exec N2 ip addr add 10.0.0.2/24 dev tap2
ip netns exec N3 ip addr add 10.0.0.1/24 dev tap3
ip netns exec N4 ip addr add 10.0.0.2/24 dev tap4
