#!/bin/bash

ip link set dev br0 down
brctl delbr br0
ip link del dev br-tap1
ip link del dev br-tap2
ip netns del N1
ip netns del N2
