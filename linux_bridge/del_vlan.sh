#!/bin/bash

ip link set dev br0 down
brctl delbr br0
ip link del dev br-tap1
ip link del dev br-tap2
ip link del dev br-tap3
ip link del dev br-tap4

ip netns del N1
ip netns del N2
ip netns del N3
ip netns del N4
